﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace HipChat
{
    public class OnlineStateImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var state = (OnlineState)value;

            switch (state)
            {
                case OnlineState.Offline:
                    return "/Assets/statuses/status-offline.png";
                case OnlineState.Away:
                case OnlineState.ExtendedAway:
                    return "/Assets/statuses/status-away.png";
                case OnlineState.DoNotDisturb:
                    return "/Assets/statuses/status-busy.png";
                case OnlineState.OnMobileApple:
                    return "/Assets/statuses/status-mobile-apple.png";
                case OnlineState.OnMobileAndroid:
                    return "/Assets/statuses/status-mobile-android.png";
                case OnlineState.PublicRoom:
                    return "/Assets/statuses/status-public-room.png";
            }

            return "/Assets/statuses/status-available.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
